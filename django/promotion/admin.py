from django.contrib import admin
from .models import Promotion, PromotionProduct
# Register your models here.
admin.site.register(Promotion)
admin.site.register(PromotionProduct)