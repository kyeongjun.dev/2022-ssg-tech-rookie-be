from django.shortcuts import render
from django.core.serializers import serialize
from django.http import HttpResponse, JsonResponse, HttpResponseNotFound
from .models import Promotion
import json
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

# Create your views here.
@method_decorator(csrf_exempt)
def index(request):
    # 전체 프로모션 출력
    if request.method == 'GET': 
        promotions = serialize("json", Promotion.objects.all())
        return HttpResponse(promotions, content_type=u"application/json; charset=utf-8")
    # 사용자 추가
    elif request.method == 'POST':
        request = json.loads(request.body)
        promotion = Promotion(
            promotion_name = request['name'],
            discount_amount = request['amount'],
            discount_rate = request['rate'],
            promotion_start = request['startdate'],
            promotion_end = request['enddate']
        )
        promotion.save()
        return HttpResponse(status=200)

@method_decorator(csrf_exempt)
def get(request, promotion_id):
    # promotion_id에 해당하는 프로모션 리턴
    if request.method == 'GET':
        promotion = Promotion.objects.filter(pk=promotion_id)
        if promotion.exists():
            promotion = serialize("json", promotion)
            return HttpResponse(promotion, content_type=u"application/json; charset=utf-8")
        else:
            return HttpResponseNotFound("Promotion does not exist")

    # promotion_id에 해당하는 프로모션 제거
    if request.method == 'DELETE':
        promotion = Promotion.objects.filter(pk=promotion_id)
        if promotion.exists():
            promotion.delete()
            return HttpResponse(status=200)
        else:
            return HttpResponseNotFound("Promotion does not exist")