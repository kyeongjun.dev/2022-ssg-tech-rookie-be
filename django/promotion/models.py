from django.db import models
from product.models import Product
# Create your models here.
class Promotion(models.Model):
    promotion_name = models.CharField(max_length=10)
    discount_amount = models.IntegerField(blank=True, null=True)
    discount_rate = models.FloatField(blank=True, null=True)
    promotion_start = models.DateField()
    promotion_end = models.DateField()
    def __str__(self):
        return self.promotion_name

class PromotionProduct(models.Model):
    promotion_id = models.ForeignKey(Promotion, on_delete=models.CASCADE)
    product_list = models.TextField()