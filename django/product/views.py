from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound
from django.core.serializers import serialize
import json
from .models import Product
from promotion.models import Promotion, PromotionProduct
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
import datetime

# Create your views here.
@method_decorator(csrf_exempt)
def index(request):
    # 전체 물품 출력
    if request.method == 'GET': 
        users = serialize("json", Product.objects.all())
        return HttpResponse(users, content_type=u"application/json; charset=utf-8")
    # 물품 추가
    elif request.method == 'POST':
        request = json.loads(request.body)
        product = Product(
            product_name = request['name'],
            product_type = request['type'],
            product_price = request['price'],   
            product_display_start = request['startdate'],
            product_display_end = request['enddate'],
        )
        product.save()
        return HttpResponse(status=200)

@method_decorator(csrf_exempt)
def get(request, product_id):
    # product_id에 해당하는 상품이 있으면 상품 정보 반환
    if request.method == 'GET':
        product_obj = Product.objects.filter(pk=product_id)
        if product_obj.exists():
            product = serialize("json", product_obj)
        else:
            return HttpResponseNotFound("Product does not exist")
        product_fields = json.loads(product)[0]["fields"]
        product_price = int(product_fields['product_price'])
        # 프로모션 중, product_id에 해당하는 상품이 있는지 체크
        avail_promotions = []
        promotion_products = PromotionProduct.objects.all()
        for promotion_product in promotion_products:
            product_set = set(map(int, promotion_product.product_list.split(',')))
            if product_id in product_set:
                avail_promotions.append(promotion_product.promotion_id.pk)
        
        # 현재 날짜 생성
        date_now = datetime.date.today()
        
        # 적용 가능한 프로모션 중, 현재 날짜와 비교하여 가능한 프로모션으로 갱신
        # 프로모션 적용된 가격들 중 0 이상이고 가장 적은 가격을 저장
        result_price = 1e9
        low_promotion = -1
        for promotion_id in avail_promotions:
            promotion = Promotion.objects.get(pk=promotion_id)
            low_price = 1e9
            if promotion.promotion_start <= date_now <= promotion.promotion_end:
                amount_price = None
                if promotion.discount_amount != None:
                    amount_price = product_price - int(promotion.discount_amount)
                rate_price = None
                if promotion.discount_rate != None:
                    rate_price = product_price - int(product_price * promotion.discount_rate)
                
                if amount_price != None:
                    low_price = amount_price
                elif rate_price != None:
                    low_price = rate_price
                if low_price <= 0:
                    low_price = product_price
                else:
                    pass
            if result_price > low_price:
                low_promotion = promotion
                result_price = low_price
        d = dict()
        d['origin'] = product
        d['promotion_price'] = result_price
        d['promotion'] = low_promotion.promotion_name
        return HttpResponse(json.dumps(d, ensure_ascii=False), content_type=u"application/json; charset=utf-8", )

    # product_id에 해당하는 상품 제거
    if request.method == 'DELETE':
        product = Product.objects.filter(pk=product_id)
        if product.exists():
            product.delete()
            return HttpResponse(status=200)
        else:
            return HttpResponseNotFound("Product does not exist")