from django.db import models

# Create your models here.
class Product(models.Model):
    product_name = models.CharField(max_length=10)
    product_type = models.CharField(max_length=10)
    product_price = models.CharField(max_length=10)
    product_display_start = models.DateField()
    product_display_end = models.DateField()
    def __str__(self):
        return self.product_name