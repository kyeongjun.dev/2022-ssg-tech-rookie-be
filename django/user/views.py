from django.shortcuts import render
from django.core.serializers import serialize
from django.http import HttpResponse, JsonResponse, HttpResponseNotFound
from .models import User
from product.models import Product
import json
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
import datetime
# Create your views here.
@method_decorator(csrf_exempt)
def index(request):
    # 전체 사용자 출력
    if request.method == 'GET': 
        users = serialize("json", User.objects.all())
        return HttpResponse(users, content_type=u"application/json; charset=utf-8")
    # 사용자 추가
    elif request.method == 'POST':
        request = json.loads(request.body)
        user = User(
            user_name = request['name'],
            user_type = request['type'],
            user_stat = request['stat']
        )
        user.save()
        return HttpResponse(status=200)

@method_decorator(csrf_exempt)
def get(request, user_id):
    # user_id에 해당하는 사용자가 구매할 수 있는 상품 목록
    if request.method == 'GET':
        user = User.objects.filter(pk=user_id)
        if user.exists():
            user = serialize("json", user)
        else:
            return HttpResponseNotFound("User does not exist")
        user_fields = json.loads(user)[0]["fields"]
        user_type = user_fields["user_type"]
        user_stat = user_fields["user_stat"]
        # 탈퇴한 회원이면 탈퇴했다는 문구 리턴
        if user_stat == "탈퇴":
            return HttpResponse("탈퇴한 회원입니다")
        # 정상 회원이면 진행
        elif user_stat == "정상":
            # 현재 날짜 생성
            date_now = datetime.date.today()
            # 구매가능한 상품을 저장하는 리스트
            avail_products = []
            if user_type == "일반":
                # 일반 사용자가 구매 가능한 상품 목록 탐색
                products = Product.objects.filter(product_type='일반')
                for product in products:
                    # product의 전시 기간이 현재 기간 기준에 만족하는지 판단
                    # 구매 가능하다면, avail_products 리스트에 추가
                    if product.product_display_start <= date_now <= product.product_display_end:
                        avail_products.append(product)
            elif user_type == "기업회원":
                # 기업 사용자가 구매 가능한 상품 목록
                products = Product.objects.filter(product_type='기업회원상품')
                for product in products:
                    # product의 전시 기간이 현재 기간 기준에 만족하는지 판단
                    # 구매 가능하다면, avail_products 리스트에 추가
                    if product.product_display_start <= date_now <= product.product_display_end:
                        avail_products.append(product)
            print(serialize("json", avail_products))
            return HttpResponse(serialize("json", avail_products), content_type=u"application/json; charset=utf-8")

    # user_id에 해당하는 사용자 제거
    if request.method == 'DELETE':
        user = User.objects.filter(pk=user_id)
        if user.exists():
            user.delete()
            return HttpResponse(status=200)
        else:
            return HttpResponseNotFound("User does not exist")