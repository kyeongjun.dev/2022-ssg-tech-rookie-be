# 2022-ssg-tech-rookie-BE

## git 주소
https://gitlab.com/kyeongjun.dev/2022-ssg-tech-rookie-be/

## Getting started

1. django/create_secret.py 파일을 실행시켜 secret key를 생성
2. cp secret.json.template secret.json
3. secret.json에 secret key 입력
```
from
{
    "SECRET_KEY": ""
}

to
{
    "SECRET_KEY": "nv>S$%Qv@e1hfm__Y=1&G,tH51)i6qo7QX>.je?I/BrO=I>N`;"
}
```
4. pip install django==4.0.5
5. python manage.py migrate --run-syncdb
6. python manage.py runserver


## 요청 url
- http://localhost/user/

GET 모든 사용자들 정보 출력

POST 사용자 정보를 입력받아 사용자 추가

ex)
```
{
    "name": "이수경",
    "type": "일반",
    "stat": "탈퇴"
}
```



- http://localhost/user/<int:user_id>

GET user_id에 해당하는 사용자가 구매할 수 있는 상품 목록 출력(일반, 기업회원 별)

DELETE user_id에 해당하는 사용자 삭제



- http://localhost/product/

GET 모든 상품들 정보 출력

POST 상품 정보를 입력받아 상품 추가
ex)
```
{
    "name":"크리스마스 케이크",
    "type":"일반",
    "price":"30000",
    "startdate":"2022-12-24",
    "enddate":"2022-12-31"
}
```


- http://localhost/product/<int:product_id>

GET product_id에 해당하는 상품이 포함되는 모든 프로모션 가격 중, 가장 낮은 가격과 프로모션 정보를 상품 정보와 함께 출력

DELETE product_id에 해당하는 상품 삭제



- http://localhost/promotion/
GET 모든 프로모션 정보 출력

POST 프로모션 정보를 입력받아 프로모션 추가
ex)
```
{
    "name":"할인행사2",
    "amount":3000,
    "rate":0,
    "startdate":"2022-01-01",
    "enddate":"2023-01-01"
}
```


- http://localhost/promotion/<int:promotion_id>

GET promotion_id에 해당하는 프로모션 정보 출력

DELETE promotion_id에 해당하는 프로모션 삭제
